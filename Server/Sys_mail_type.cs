﻿/*
* Sys_mail_type.cs
*
* 功 能： N/A
* 类 名： Sys_mail_type
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;
using System.Text;

namespace XHD.Server
{
    public class Sys_mail_type
    {
        public static BLL.Sys_mail_type mailtype = new BLL.Sys_mail_type();
        public static Model.Sys_mail_type model = new Model.Sys_mail_type();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_mail_type()
        {
        }

        public Sys_mail_type(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void save()
        {
            model.mail_type = PageValidate.InputText(request["T_name"], 50);
            model.smtp = PageValidate.InputText(request["T_smtp"], 50);

            if (PageValidate.IsNumber(request["T_port"]))
                model.smtp_port = int.Parse(request["T_port"]);

            if (PageValidate.IsNumber(request["T_ssl_val"]))
                model.use_ssl = int.Parse(request["T_ssl_val"]);

            if (PageValidate.IsNumber(request["T_order"]))
                model.mail_order = int.Parse(request["T_order"]);

            string id = PageValidate.InputText(request["id"], 50);
            if (PageValidate.IsNumber(id))
            {
                model.id = int.Parse(id);

                DataSet ds = mailtype.GetList(string.Format("id={0}", id));
                DataRow dr = ds.Tables[0].Rows[0];

                mailtype.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.mail_type;
                ;
                string EventType = "邮箱类别修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["mail_type"].ToString() != request["T_name"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "类别", dr["mail_type"], request["T_name"]);

                if (dr["smtp"].ToString() != request["T_smtp"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "smtp", dr["smtp"], request["T_smtp"]);

                if (dr["smtp_port"].ToString() != request["T_port"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "smtp端口", dr["smtp_port"], request["T_port"]);

                if (dr["use_ssl"].ToString() != request["T_ssl"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "ssl", dr["use_ssl"], request["T_ssl"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                mailtype.Add(model);
            }
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " mail_order ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " asc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";


            //if (!string.IsNullOrEmpty(request["customerid"]))
            //    serchtxt += " and C_customerid=" + int.Parse(request["customerid"]);

            //context.Response.Write(serchtxt);

            DataSet ds = mailtype.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);

            return dt;
        }

        public string form(string id)
        {
            string dt;
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = mailtype.GetList(string.Format("id = {0}", int.Parse(id)));
                dt = DataToJson.DataToJSON(ds);
            }
            else dt = "{}";

            return dt;
        }

        public string del(int id)
        {
            DataSet ds = mailtype.GetList(string.Format("id = {0}", id));
            string EventType = "邮箱类别删除";

            bool isdel = mailtype.Delete(id);
            if (isdel)
            {
                //日志
                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                int EventID = id;
                string EventTitle = ds.Tables[0].Rows[0]["mail_type"].ToString();

                var log = new sys_log();
                log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID,
                    ds.Tables[0].Rows[0]["mail_type"].ToString());

                return "true";
            }
            return "false";
        }
        //combo
        public string combo()
        {
            DataSet ds = mailtype.GetList(0, "", "mail_order");

            var str = new StringBuilder();

            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["mail_type"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }

    }
}