﻿/*
* CRM_SMS.cs
*
* 功 能： N/A
* 类 名： CRM_SMS
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/19 21:46:10    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
    /// <summary>
    /// CRM_SMS:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CRM_SMS
    {
        public CRM_SMS()
        { }
        #region Model
        private int _id;
        private string _sms_title;
        private string _sms_content;
        private string _contact_ids;
        private string _sms_mobiles;
        private int? _issend;
        private DateTime? _sendtime;
        private int? _check_id;
        private int? _dep_id;
        private int? _create_id;
        private DateTime? _create_time;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sms_title
        {
            set { _sms_title = value; }
            get { return _sms_title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sms_content
        {
            set { _sms_content = value; }
            get { return _sms_content; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string contact_ids
        {
            get { return _contact_ids; }
            set { _contact_ids = value; }
        }

        public string sms_mobiles
        {
            get { return _sms_mobiles; }
            set { _sms_mobiles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? isSend
        {
            set { _issend = value; }
            get { return _issend; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? sendtime
        {
            set { _sendtime = value; }
            get { return _sendtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? check_id
        {
            set { _check_id = value; }
            get { return _check_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? dep_id
        {
            set { _dep_id = value; }
            get { return _dep_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? create_time
        {
            set { _create_time = value; }
            get { return _create_time; }
        }
        #endregion Model

    }
}

