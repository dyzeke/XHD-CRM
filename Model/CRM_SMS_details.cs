﻿/*
* CRM_SMS_details.cs
*
* 功 能： N/A
* 类 名： CRM_SMS_details
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/19 21:46:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
	/// <summary>
	/// CRM_SMS_details:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CRM_SMS_details
	{
		public CRM_SMS_details()
		{}
		#region Model
		private int? _sms_id;
		private int? _contact_id;
		/// <summary>
		/// 
		/// </summary>
		public int? sms_id
		{
			set{ _sms_id=value;}
			get{return _sms_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? contact_id
		{
			set{ _contact_id=value;}
			get{return _contact_id;}
		}
		#endregion Model

	}
}

