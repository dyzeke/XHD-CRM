/*
* Sys_data_authority.cs
*
* 功 能： N/A
* 类 名： Sys_data_authority
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_data_authority:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_data_authority
    {
        #region Model

        private DateTime? _create_date;
        private int? _create_id;
        private int? _option_id;
        private int? _role_id;
        private int? _sys_add;
        private int? _sys_del;
        private int? _sys_edit;
        private string _sys_option;
        private int? _sys_view;

        /// <summary>
        /// </summary>
        public int? Role_id
        {
            set { _role_id = value; }
            get { return _role_id; }
        }

        /// <summary>
        /// </summary>
        public int? option_id
        {
            set { _option_id = value; }
            get { return _option_id; }
        }

        /// <summary>
        /// </summary>
        public string Sys_option
        {
            set { _sys_option = value; }
            get { return _sys_option; }
        }

        /// <summary>
        /// </summary>
        public int? Sys_view
        {
            set { _sys_view = value; }
            get { return _sys_view; }
        }

        /// <summary>
        /// </summary>
        public int? Sys_add
        {
            set { _sys_add = value; }
            get { return _sys_add; }
        }

        /// <summary>
        /// </summary>
        public int? Sys_edit
        {
            set { _sys_edit = value; }
            get { return _sys_edit; }
        }

        /// <summary>
        /// </summary>
        public int? Sys_del
        {
            set { _sys_del = value; }
            get { return _sys_del; }
        }

        /// <summary>
        /// </summary>
        public int? Create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        #endregion Model
    }
}