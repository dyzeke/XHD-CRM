﻿/*
* Sys_mail_list.cs
*
* 功 能： N/A
* 类 名： Sys_mail_list
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-07-20 09:50:36    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_mail_list:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_mail_list
    {
        #region Model

        private int? _belong_id;
        private string _email;
        private int _id;
        private int? _mail_enable;
        private int? _mail_type_id;
        private string _password;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public int? mail_type_id
        {
            set { _mail_type_id = value; }
            get { return _mail_type_id; }
        }

        /// <summary>
        /// </summary>
        public string email
        {
            set { _email = value; }
            get { return _email; }
        }

        /// <summary>
        /// </summary>
        public string password
        {
            set { _password = value; }
            get { return _password; }
        }

        /// <summary>
        /// </summary>
        public int? belong_id
        {
            set { _belong_id = value; }
            get { return _belong_id; }
        }

        /// <summary>
        /// </summary>
        public int? mail_enable
        {
            set { _mail_enable = value; }
            get { return _mail_enable; }
        }

        #endregion Model
    }
}