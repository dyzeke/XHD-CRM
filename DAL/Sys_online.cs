/*
* Sys_online.cs
*
* 功 能： N/A
* 类 名： Sys_online
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_online
    /// </summary>
    public class Sys_online
    {
        #region  Method

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public void Add(Model.Sys_online model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into Sys_online(");
            strSql.Append("UserID,UserName,LastLogTime)");
            strSql.Append(" values (");
            strSql.Append("@UserID,@UserName,@LastLogTime)");
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserID", SqlDbType.Int, 4),
                new SqlParameter("@UserName", SqlDbType.VarChar, 50),
                new SqlParameter("@LastLogTime", SqlDbType.DateTime)
            };
            parameters[0].Value = model.UserID;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.LastLogTime;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.Sys_online model, string where)
        {
            var strSql = new StringBuilder();
            strSql.Append("update Sys_online set ");
            strSql.Append("UserID=@UserID,");
            strSql.Append("UserName=@UserName,");
            strSql.Append("LastLogTime=@LastLogTime");
            strSql.Append(" where " + where);
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserID", SqlDbType.Int, 4),
                new SqlParameter("@UserName", SqlDbType.VarChar, 50),
                new SqlParameter("@LastLogTime", SqlDbType.DateTime)
            };
            parameters[0].Value = model.UserID;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.LastLogTime;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(string wherestr)
        {
            //该表无主键信息，请自定义主键/条件字段
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_online ");
            strSql.Append(" where " + wherestr + "  ");
            SqlParameter[] parameters =
            {
            };

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select UserID,UserName,LastLogTime ");
            strSql.Append(" FROM Sys_online ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" UserID,UserName,LastLogTime ");
            strSql.Append(" FROM Sys_online ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }
}