﻿/*
* MSG_Task.cs
*
* 功 能： N/A
* 类 名： MSG_Task
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/7/28 16:09:43    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     MSG_Task
    /// </summary>
    public class MSG_Task
    {
        private readonly DAL.MSG_Task dal = new DAL.MSG_Task();

        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.MSG_Task model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.MSG_Task model)
        {
            return dal.Update(model);
        }
        
        /// <summary>
        ///     更新状态
        /// </summary>
        public bool UpdateStatu(Model.MSG_Task model)
        {
            return dal.UpdateStatu(model);
        }

        
        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            return dal.Delete(id);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(PageValidate.SafeLongFilter(idlist, 0));
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}