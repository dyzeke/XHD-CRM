/*
* Sys_data_authority.cs
*
* 功 能： N/A
* 类 名： Sys_data_authority
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;

namespace XHD.BLL
{
    /// <summary>
    ///     Sys_data_authority
    /// </summary>
    public class Sys_data_authority
    {
        private readonly DAL.Sys_data_authority dal = new DAL.Sys_data_authority();

        #region  Method

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public void Add(Model.Sys_data_authority model)
        {
            dal.Add(model);
        }


        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(string where)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete(where);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        #endregion  Method
    }
}