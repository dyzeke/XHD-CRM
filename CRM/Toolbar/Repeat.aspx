<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="../../lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/input.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTree.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"> </script>
    <script src="../../lib/jquery.form.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"> </script>
    <script src="../../JS/XHD.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"> </script>
    <script type="text/javascript">
        if (top.location == self.location) top.location = "../../default.aspx";
        var manager;
        var manager1;
        $(function () {

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            grid();
            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('form').ligerForm();
            toolbar();


            //var tt = test(jsonObj.Rows, 3);
            //alert(JSON.stringify(tt));
        });
       
        function grid() {
            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '序号', name: 'n', width: 50 },
                    {
                        display: '公司名称', name: 'Customer', width: 200, align: 'left', render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view(1," + item.id + ")>";
                            if (item.Customer)
                                html += item.Customer;
                            html += "</a>";
                            return html;
                        }
                    },
                    { display: '电话', name: 'tel', width: 120, align: 'right' },
                    { display: '客户类型', name: 'CustomerType', width: 80 },
                    { display: '客户类别', name: 'CustomerLevel', width: 80 },
                    { display: '客户来源', name: 'CustomerSource', width: 80 },
                    { display: '省份', name: 'Provinces', width: 80 },
                    { display: '城市', name: 'City', width: 80 },
                    { display: '所属行业', name: 'industry', width: 80 },
                    { display: '部门', name: 'Department', width: 80 },
                    { display: '员工', name: 'Employee', width: 80 },
                    { display: '客源状态', name: 'privatecustomer', width: 60 },
                    {
                        display: '最后跟进', name: 'lastfollow', width: 90, render: function (item) {
                            var lastfollow = formatTimebytype(item.lastfollow, 'yyyy-MM-dd');
                            if (lastfollow == "1900-01-01")
                                lastfollow = "";
                            return lastfollow;
                        }
                    },
                    {
                        display: '创建时间',
                        name: 'Create_date',
                        width: 90,
                        render: function (item) {
                            var Create_date = formatTimebytype(item.Create_date, 'yyyy-MM-dd');
                            return Create_date;
                        }
                    }
                ],
                onBeforeShowData: function (grid, data) {
                    startTime = new Date();
                },                    
                rowtype: "CustomerType",
                dataAction: 'server',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "CRM_Customer.grid.xhd?type=repeat&rnd=" + Math.random(),
                width: '100%',
                height: '100%',
                heightDiff: -1,
                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                onAfterShowData: function (grid) {
                    $("tr[rowtype='已成交']").addClass("l-treeleve1").removeClass("l-grid-row-alt");
                    var nowTime = new Date();
                    //alert('加载数据耗时：' + (nowTime - startTime));
                },
                groupColumnName: 'Customer',
                groupColumnDisplay: '客户'

            });
          
        }

        function toolbar() {
            $.getJSON("toolbar.GetSys.xhd?mid=58&rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({
                    type: 'serchbtn',
                    text: '高级搜索',
                    icon: '../../images/search.gif',
                    disable: true,
                    click: function () {
                        serchpanel();
                    }
                });
                items.push({ type: "filter", icon: '../../images/icon/51.png', title: "帮助", click: function () { help(); } })
                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu = $.ligerMenu({
                    width: 120,
                    items: getMenuItems(data)
                });
$("#maingrid4").ligerGetGridManager().onResize();
            });
           

                
            
        }

        function initSerchForm() {
            var a = $('#T_City').ligerComboBox({ width: 96, emptyText: '（空）' });
            var b = $('#T_Provinces').ligerComboBox({
                width: 97,

                url: "Param_City.combo1&rnd=" + Math.random(),
                onSelected: function (newvalue) {
                    $.get("Param_City.combo2.xhd?pid=" + newvalue + "&rnd=" + Math.random(), function (data, textStatus) {
                        a.setData(eval(data));
                    });
                },
                emptyText: '（空）'
            });
            $('#customertype').ligerComboBox({ width: 97, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=1&rnd=" + Math.random() });
            $('#customerlevel').ligerComboBox({ width: 96, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=2&rnd=" + Math.random() });
            $('#cus_sourse').ligerComboBox({ width: 196, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=3&rnd=" + Math.random() });
            $('#industry').ligerComboBox({ width: 120, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=8&rnd=" + Math.random() });
            var e = $('#employee').ligerComboBox({ width: 96, emptyText: '（空）' });
            var f = $('#department').ligerComboBox({
                width: 97,
                selectBoxWidth: 240,
                selectBoxHeight: 200,
                valueField: 'id',
                textField: 'text',
                treeLeafOnly: false,
                tree: {
                    url: 'hr_department.tree.xhd?rnd=' + Math.random(),
                    idFieldName: 'id',
                    //parentIDFieldName: 'pid',
                    checkbox: false
                },
                onSelected: function (newvalue) {
                    $.get("hr_employee.combo.xhd?did=" + newvalue + "&rnd=" + Math.random(), function (data, textStatus) {
                        e.setData(eval(data));
                    });
                }
            });
        }

        function serchpanel() {
            initSerchForm();
            if ($(".az").css("display") == "none") {
                $("#grid").css("margin-top", $(".az").height() + "px");
                $("#maingrid4").ligerGetGridManager().onResize();
            } else {
                $("#grid").css("margin-top", "0px");
                $("#maingrid4").ligerGetGridManager().onResize();
            }
            $("#company").focus();
        }

        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });

        function doserch() {
            var sendtxt = "&type=repeat&&rnd=" + Math.random();
            var serchtxt = $("#serchform :input").fieldSerialize() + sendtxt;
            //alert(serchtxt);           
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.GetDataByURL("CRM_Customer.grid.xhd?" + serchtxt);

        }

        function doclear() {
            $("input:hidden", "#serchform").val("");
            $("input:text", "#serchform").val("");
            $(".l-selected").removeClass("l-selected");
        }

        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "CRM_Customer.AdvanceDelete.xhd",
                            type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            success: function (responseText) {
                                if (responseText == "true") {
                                    f_reload();
                                } else if (responseText == "delfalse") {
                                    top.$.ligerDialog.error('权限不够，删除失败！');
                                } else if (responseText == "false") {
                                    top.$.ligerDialog.error('未知错误，删除失败！');
                                } else {
                                    top.$.ligerDialog.warn('此客户下含有 ' + responseText + '，删除失败！请先先将这些数据删除。');
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                });
            } else {
                $.ligerDialog.warn("请选择客户");
            }
        }

       

        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }
       

        function help(parameters) {
            $.ligerDialog.question("此界面是小黄豆CRM的客户管理界面，上面部分是客户列表，下面部分是跟进。点击客户，可以加载跟进信息。具体请访问官方教程：<br/><br/><a href='http://www.xhdcrm.com/khgl/8.html' target='_blank'>客户管理：客户列表教程</a>", "提示");
            //$.ligerDialog.open({ height: 200, url: 'http://wwww.xhdcrm.com', width: null });
        }
    </script>
    <style type="text/css">
        .l-treeleve1 { background: yellow; }

        .l-treeleve2 { background: yellow; }

        .l-treeleve3 { background: #eee; }
    </style>
</head>
<body>
    <form id="form1" onsubmit=" return false ">
        <div id="toolbar"></div>

        <div id="grid">
            <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            <div id="toolbar1"></div>
            <div id="Div1" style="position: relative;">
                <div id="maingrid5" style="margin: -1px -1px;"></div>
            </div>
        </div>


    </form>
    <div class="az">
        <form id='serchform'>
            <table style='width: 960px' class="bodytable1">
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>公司名称：</div>
                    </td>
                    <td>
                        <input type='text' id='company' name='company' ltype='text' ligerui='{width:120}' /></td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>客户类型：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='customertype' name='customertype' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='customerlevel' name='customerlevel' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>录入时间：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startdate' name='startdate' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='enddate' name='enddate' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>
                    <td>
                        <input id='keyword' name="keyword" type='text' ltype='text' ligerui='{width:196, nullText: "输入关键词搜索地址、描述、备注"}' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>所属行业：</div>
                    </td>
                    <td>
                        <input id='industry' name="industry" type='text' /></td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>所属地区：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='T_Provinces' name='T_Provinces' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='T_City' name='T_City' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>最后跟进：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startfollow' name='startfollow' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='endfollow' name='endfollow' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>

                </tr>
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>电话：</div>
                    </td>
                    <td>
                        <input type='text' id='tel' name='tel' ltype='text' ligerui='{width:120}' />
                    </td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>客户来源：</div>
                    </td>
                    <td>
                        <input type='text' id='cus_sourse' name='cus_sourse' />
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>业务员：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='department' name='department' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='employee' name='employee' />
                        </div>
                    </td>
                    <td>

                        <input id='Button2' type='button' value='重置' style='height: 24px; width: 80px;'
                            onclick=" doclear() " />
                        <input id='Button1' type='button' value='搜索' style='height: 24px; width: 80px;' onclick=" doserch() " />
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <form id='toexcel'></form>
</body>
</html>
