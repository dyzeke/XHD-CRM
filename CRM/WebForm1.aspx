﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="XHD.View.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="lib/jquery/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#test").bind("click",test)
        })
        function test() {
            $.ajax({
                type: "post",
                // url: "data/ajax.ashx?xhd=Ajaxtest.tt", /* 注意后面的名字对应CS的方法名称 */
                url:"/Ajaxtest/tt.xhd",
                data: { page: $("#a").val(), b: $("#b").val(), rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                //dataType: "json",
                success: function (result) {
                    $("#view").val(result);
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <input type="text" id="a" />
        <input type="text" id="b" />
        <input type="button" id="test" value="test" />
        <textarea id="view"></textarea>
    </div>
    </form>
</body>
</html>
